import React from "react";
import {Button} from '@holism/core';
import {InternetIcon, MinusIcon, PlusIcon} from '@holism/icons';
import {COLORS} from '@holism/core';

const Buttons = () => {
    return (
        <div className={"buttons-wrapper"}>
            <div><Button className={"button-control"} dimension="medium" isWithIcon={true}><PlusIcon
                color={COLORS.white} size={20}/>
                <span className={"none"}> Приблизить </span></Button></div>
            <div><Button className={"button-control"} dimension="medium" isWithIcon={true}><MinusIcon
                color={COLORS.white} size={20}/>
                <span className={"none"}>Отдалить </span></Button></div>
            <div><Button className={"button-control"} dimension="medium" isWithIcon={true}><InternetIcon
                color={COLORS.white} size={20}/>
                <span className={"none"}>Тепловая карта </span></Button></div>
        </div>
    )
}
export default Buttons;