import React, { Component } from "react";
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import MarkerClusterGroup from "react-leaflet-markercluster";


class pointMain extends Component {
  constructor(props) {
      super(props);
      this.state = {
        data: [],
        loaded: false,
        placeholder: "Loading",
        zoom: 13,
    };
  };

  componentDidMount() {
    fetch("/api/v1/point/")
        .then(response => {
            if (response.status > 400) {
                return this.setState(() => {
                    return {placeholder: "Something went wrong!",};
                });
            }
            return response.json();
        })
        .then(data => {
            this.setState(() => {
              return {
                  data,
                  loaded: true,
                };
            });
        });
}
coordMap() {
return(
  <div>
      {this.state.data.map(coord => {
    const position = [coord.lat, coord.long]
    return(
      <Marker position={position}>
        <Popup key={coord.id}>
          {coord.address}
        </Popup>
      </Marker>
      )
    }
  )
}
  </div>
)
}

coordMapCreatePlacemark() {
  return (
    <MarkerClusterGroup>
    {this.coordMap()}
    </MarkerClusterGroup>
  )
}


  render() {
    return this.coordMapCreatePlacemark()
}
}

export default pointMain;