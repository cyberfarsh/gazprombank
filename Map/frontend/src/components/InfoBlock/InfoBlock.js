import React from "react";
import {AtmIcon, BankIcon, InvestmentIcon} from '@holism/icons';
import {COLORS, Grid, GridCol} from '@holism/core';

const InfoBlock = () => {
    return (
        <div className={"info-block-wrapper"}>
            <div className={"info-block"}>

                <div className={"title-card separator"}>
                    Данные по Москве
                </div>

                <Grid className={"card-area"}>
                    <GridCol className={"card"}>
                        <div className={"card-name"}><AtmIcon color={COLORS.blue} size={30}/> <span
                            className={"card-title"}>Количество<br/>банкоматов</span></div>
                        <div className={"number"}>~192</div>
                    </GridCol>
                    <GridCol className={"card"}>
                        <div className={"card-name"}><BankIcon color={COLORS.blue} size={30}/> <span
                            className={"card-title"}>Количество<br/>объектов</span></div>
                        <div className={"number"}>~32127</div>
                    </GridCol>
                    <GridCol className={"card"}>
                        <div className={"card-name"}><InvestmentIcon color={COLORS.blue} size={30}/> <span
                            className={"card-title"}>Статистика</span></div>
                        <div className={"number"}>???</div>
                    </GridCol>
                </Grid>

            </div>
        </div>
    )
}
export default InfoBlock;