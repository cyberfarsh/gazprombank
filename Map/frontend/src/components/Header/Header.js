import React from "react";

const Header = () => {
    return (
        <header>
            <div className={"logo"}>
                <img src={"/static/images/white-logo.svg"} alt={""}/>
            </div>
        </header>
    )
}
export default Header;