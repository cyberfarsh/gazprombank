# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Atm(models.Model):
    id = models.BigIntegerField(primary_key=True)
    geolocationlatitude = models.FloatField(blank=True, null=True)
    geolocationlongitude = models.FloatField(blank=True, null=True)
    addressregion = models.TextField(blank=True, null=True)
    addressregiontype = models.TextField(blank=True, null=True)
    addresssettlementtype = models.TextField(blank=True, null=True)
    addresssettlement = models.TextField(blank=True, null=True)
    addressfulladdress = models.TextField(blank=True, null=True)
    addressnearestsubway = models.TextField(blank=True, null=True)
    addresslocation = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'atm'


class mosHouse(models.Model):
    id = models.IntegerField(primary_key=True)
    living_house = models.TextField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    long = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mosHouse'


class Datapoint(models.Model):
    id = models.IntegerField(primary_key=True)
    address = models.TextField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    long = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'datapoint'